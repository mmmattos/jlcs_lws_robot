# -*- encoding: utf-8 -*-
__author__ = 'mmmattos'
#To change this template use Tools | Templates.
#

import os
from time import time
from pathlib import Path
import threading
import requests
import logging
from time import sleep

logger = logging.getLogger(__name__)

def download_link(directory, link):
    logger.info('Baixando imagem %s',link)
    try:
        r = requests.get(link, stream=True)
        directory_posix = Path(directory) / os.path.basename(link)
        with open(str(directory_posix), 'wb') as f:
            for chunk in r.iter_content(1024):
                if chunk:
                    f.write(chunk)
    except requests.exceptions.ConnectionError as e:
        logger.error(
            "Conexão rejeitada. Aguardando para nova tentativa."
            + str(e.args[0].reason)
        )
        sleep(3)

                
def download_links(download_dir, links):
    logger.info(
        'Recebendo lista de links: %d',
        links.__len__()
    )
    
    for link in links:
        catalogo = link.split('/')[-2]
        directory = setup_download_dir(str(os.path.join(download_dir, catalogo)))
        download_link(directory, link)
        

## Simple form of threaded download...                
# def createNewDownloadThread(link, filelocation):
#     download_thread = threading.Thread(target=download, args=(filelocation,link))
#     download_thread.start()


def get_links():
    #return [ 'http://www.leiloesbr.com.br/imagens/img_p/4972/2020453.jpg' ]
    return [ 'http://www.leiloesbr.com.br/imagens/img_p/4778/1967302.jpg' ]

def setup_download_dir(catalogo):
    logger.info('Chegou em setup_download_dir:' + str(catalogo))
    download_dir = Path(catalogo)
    if not download_dir.exists():
        download_dir.mkdir()
    return download_dir

def Download(dnld_dir,links):
    ts = time()
    ## Put the tasks into the queue as a tuple
    for link in links:
        catalogo = str(link).split('/')[-2];
        download_dir = setup_download_dir(os.path.join(dnld_dir,catalogo))
        logger.info('Got directory: %s' % download_dir)
        logger.info('Got link: %s' % link)
        download_link(download_dir, link)
    logger.info(str('Took {}'.format(time() - ts)))



if __name__ == '__main__':
    dnld_dir = '/home/www/antiquarium_project/static/images/'
    links = get_links()
    Download(dnld_dir, links)


