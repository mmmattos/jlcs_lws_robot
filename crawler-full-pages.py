# -*- encoding: utf-8 -*-
import re
import mechanize
import cookielib
from BeautifulSoup import BeautifulSoup
import html2text
import htmllib
import redis
import time
from leiloes_srv_logger import lsLogger
from product import Product

class leiloes_crawler:
    """
    Uma Classe definida nos padrões de apresentação da Leiloes-BR.

    :copyright: (c) 2016 by Miguel Miranda de Mattos.
    :license: 
    """


    @staticmethod
    def unescape(s):
        p = htmllib.HTMLParser(None)
        p.save_bgn()
        p.feed(s)
        return p.save_end()


    


    def get_soup_for_me(self, browser, url):
        browser.open(url)
        assert browser.viewing_html()
        html = browser.response().read()
        result_soup = BeautifulSoup(html)
        return result_soup


    


    def get_category_links(self, mapped_cats_hrefs):
        all_mcats_links = dict()
        for c in mapped_cats_hrefs:
            each_mcat_link = self.base_url + c
            if c not in all_mcats_links.keys():
               all_mcats_links[c] = each_mcat_link
        return all_mcats_links    

    



    def get_products_in_brand(self,tag,br):
        brand_href_part = tag['href']
        brand_title = tag['title']
        brand_link = self.base_url + brand_href_part
        brand_soup = self.get_soup_for_me(br,brand_link)
        return {brand_title:brand_soup.findAll('div',attrs = {'class':'tblRow'})}



    
    def get_product_description_from_soup(self,description_soup):
        for a in description_soup:
            try:
                product_desc = a.findAll('p')[0].contents[0]
            except Exception, e:
                product_desc = ''
                lsLogger.warning( "Could not get description from: %s", description_soup)

        #TODO Recursively exhaust getting soups until just text is left here. NO TAGS.
        return product_desc




    def crawl(self):
        # Register regexps to be used globally
        patFinderTitle = re.compile(r'<a[^>]*\stitle="(.*?)"')
        patFinderHref = re.compile(r'<a[^>]*\shref="(.*?)"')

        # Specify unwanted categories (or meta-categories).
        unwanted_cats = set([   '/customer-service/faq/',
                                '/shopcart.aspx',
                                '/new-products/',
                                '/gifts-specials/'])

        # Fire-up a new  browser session anf get tag soup for the page.
        crawl_me = set([   'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235976%7C68265%7C131195&url=http%3A%2F%2Fwww.casarionegroleiloes.com.br%2Fcatalogo.asp%3FNum%3D4058%26p%3Don%26dia%3D1',
                        'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235977%7C68265%7C131195&url=http%3A%2F%2Fwww.garimpoprovence.com.br%2Fcatalogo.asp%3FNum%3D4093%26p%3Don%26dia%3D1',
                        'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235978%7C68265%7C131195&url=http%3A%2F%2Fwww.galaleiloes.com.br%2Fcatalogo.asp%3FNum%3D4155%26p%3Don%26dia%3D1',
                        'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235979%7C68265%7C131195&url=http%3A%2F%2Fwww.vilmarama.com.br%2Fcatalogo.asp%3FNum%3D4189%26p%3Don%26dia%3D1',
                        'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235980%7C68265%7C131195&url=http%3A%2F%2Fwww.lagemmeleiloes.com.br%2Fcatalogo.asp%3FNum%3D4074%26p%3Don%26dia%3D1',
                        'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235981%7C68265%7C131195&url=http%3A%2F%2Fwww.estilousado.com.br%2Fcatalogo.asp%3FNum%3D4185%26p%3Don%26dia%3D1',
                        'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235983%7C68265%7C131195&url=http%3A%2F%2Fwww.jonas.lel.br%2Fcatalogo.asp%3FNum%3D3954%26p%3Don%26dia%3D1',
                        'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235984%7C68265%7C131195&url=http%3A%2F%2Fwww.leilaoromas.com.br%2Fcatalogo.asp%3FNum%3D4118%26p%3Don%26dia%3D3',
                        'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235985%7C68265%7C131195&url=http%3A%2F%2Fwww.galeriasuprema.com.br%2Fcatalogo.asp%3FNum%3D4226%26p%3Don%26dia%3D1',
                        'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235986%7C68265%7C131195&url=http%3A%2F%2Fwww.artnowleiloes.com.br%2Fcatalogo.asp%3FNum%3D4149%26p%3Don%26dia%3D4',
                        'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235987%7C68265%7C131195&url=https%3A%2F%2Fwww.levyleiloeiro.com.br%2Fcatalogo.asp%3FNum%3D254%26p%3Don%26dia%3D1',
                        'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235988%7C68265%7C131195&url=http%3A%2F%2Fwww.rmgouvealeiloes.com.br%2Fcatalogo.asp%3FNum%3D4138%26p%3Don%26dia%3D1'
                        ])


        ii = 0
        for u in crawl_me:
            ii = ii + 1
            print "<BEGIN FILE " + str(ii) + ">-------------------------------------"
            br = mechanize.Browser()
            mainsoup = self.get_soup_for_me(br,u)
            print str(mainsoup)
            print "<END FILE>-------------------------------------"
        '''
        # Extract categories from top menu.
        top_cats =  mainsoup.findAll('div',
                attrs = {'class': 'topMenuContainer'})

        mapped_cats_hrefs = set(re.findall(patFinderHref,
                    str(top_cats))) - unwanted_cats

        all_mcats_links = self.get_category_links(mapped_cats_hrefs)
        lsLogger.info("Categories found: %s", str(len(all_mcats_links)))
        # Go into each category link
        
        # main scrapping - TODO: rules to be changed.
        for c in all_mcats_links.keys():
            cat_link = all_mcats_links[c]
            try:
                soup = self.get_soup_for_me(br,cat_link)
                # Filter html to only show the category content (brandDiv).
                all_brands = soup.findAll('div',attrs = {'class': 'brandDiv'})
                
                lsLogger.info("Category %s has %s  brands.", \
                    cat_link, \
                    str(len(all_brands)))

                # Navigate products in brands
                for brand_row in all_brands:
                    brand_row_soup =  BeautifulSoup(str(brand_row))

                    for tag in brand_row_soup.findAll('a', href=True):
                        products_in_brand = self.get_products_in_brand(tag, br)
                        brand_title = products_in_brand.keys()[0]
                        #print str(type(brand_title))
                        
                        for product in products_in_brand[brand_title]:
                            description_soup = \
                                product.findAll( \
                                    'div',  \
                                    attrs = {'class':'col1 vam'})
                            
                            if len(description_soup) == 0:
                                psLogger.warning( "No description soup found!")
                                continue

                            product_desc = \
                                description_soup[0].getText()
                            #    self.get_product_description_from_soup( \
                            #        description_soup)

                            price_cell =  product.findAll('div',attrs = {'class':'col4 vam'})
                            product_price = self.unescape(price_cell[0].text)
                            
                            product_href = re.findall(patFinderHref,str(description_soup))
                            href_parts =  product_href[0].split('/')
                            
                            product_id = href_parts[-2]
                         
                            product = Product( \
                                        product_id, \
                                        c, \
                                        brand_title, \
                                        product_desc, \
                                        product_price,  \
                                        product_href[0])

                            #psLogger.info(str(product))

                            product.save_to_redis()


                        br.back()
                    
            except mechanize.LinkNotFoundError:
                lsLogger.error("Link Not found!")
                continue

            # To avoid hitting the server too hard
            time.sleep(1)

            # Go back to the Inbox
            br.back()
        '''



    def __init__(self):
        lsLogger.info("Constructing a Crawler...")

        # Define base url to explore.
        crawl_me = set([   'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235972%7C68265%7C131195&url=http%3A%2F%2Fwww.navovotem.com.br%2Fcatalogo.asp%3FNum%3D4120%26p%3Don%26dia%3D2',
                             'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235973%7C68265%7C131195&url=http%3A%2F%2Fwww.amleiloes.com%2Fcatalogo.asp%3FNum%3D4213%26p%3Don%26dia%3D1',
                             'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235974%7C68265%7C131195&url=http%3A%2F%2Fwww.leiloesalarcon.com.br%2Fcatalogo.asp%3FNum%3D4092%26p%3Don%26dia%3D1',
                             'http://leiloesbr.mktsender.net/registra_clique.php?id=H%7C235975%7C68265%7C131195&url=http%3A%2F%2Fwww.brasillivros.com.br%2Fcatalogo.asp%3FNum%3D3875%26p%3Don%26dia%3D1'])
               
        #https://www.leiloesbr.com.br/
        #http://www.cesarpapini.com.br/catalogo.asp?Num=3674&dia=1
        #http://www.conradoleiloeiro.com.br/catalogo.asp?Num=3670&dia=3
        #http://www.casaamarelaleiloes.net.br/catalogo.asp?Num=3636&dia=2
        #https://www.levyleiloeiro.com.br/catalogo.asp?Num=306&dia=2

        MAX_CONSECUTIVE_ATTEMPTS = 500
        #lsLogger.info("Base URL: %s", self.base_url)
