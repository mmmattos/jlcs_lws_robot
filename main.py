# -*- encoding: utf-8 -*-
import os
import logging
import logging.config
import platform
import yaml

logger = logging.getLogger(__name__)

def setup_logging(
    default_path='/home/antiquarium_robot/logging.yaml', 
    default_level=logging.INFO,
):
    """
        Setup logging configuration
    """

    config_path = default_path
    if os.path.exists(config_path):
        with open(config_path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


setup_logging('./logging.yaml', logging.DEBUG)

logger.info("Iniciando o LOG do ANTIQUARIUM ROBOT")

from crawler import *
#os.environ["DEBUG"] = "1"
scrap = leiloes_crawler()
scrap.crawl()
logger.info("Encerrando as atividades do ANTIQUARIUM ROBOT")
