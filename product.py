# -*- encoding: utf-8 -*-
import redis
import hashlib
from pysolr import Solr, SolrError
import codecs
import socket, errno
from BeautifulSoup import BeautifulSoup
import logging
from tools import unescape
from download import Download
#from single import Download
from redis import ResponseError

module_logger = logging.getLogger('antiquarium_robot.product')


class Product:
    """
    A Classe Produto representa os produtos oferecidos e
    capturados nos sites de leilões padrão LeilõesBR.

    :copyright: (c) 2012 by Miguel Miranda de Mattos.
    :license:
    """


    def __init__(self,
                 id_param,
                 casa_leilao_param,
                 catalogo_num_param,
                 titulo_catalogo_param,
                 qualificacao_param,
                 data_hora_coleta_param,
                 card_data_hora_leilao_param,
                 data_hora_leilao_param,
                 lote_param,
                 descricao_param,
                 localizacao_param,
                 lances_param,
                 visitas_param,
                 preco_inicial_param,
                 preco_final_param,
                 url_param,
                 imagem_url_param
                ):

        """Construtor de Produto"""

        self.pId                    = id_param
        self.casa_leilao            = casa_leilao_param
        self.catalogo_num           = catalogo_num_param
        self.titulo_catalogo        = titulo_catalogo_param
        self.qualificacao           = qualificacao_param
        self.data_hora_coleta       = data_hora_coleta_param
        self.card_data_hora_leilao  = card_data_hora_leilao_param
        self.data_hora_leilao       = data_hora_leilao_param
        self.lote                   = lote_param
        self.descricao              = descricao_param
        self.localizacao            = localizacao_param
        self.lances                 = lances_param
        self.visitas                = visitas_param
        self.preco_inicial          = preco_inicial_param
        self.preco_final            = preco_final_param
        self.url                    = url_param
        self.imagem_url             = imagem_url_param

        self.logger = logging.getLogger(
          self.__module__ + '.' +
          self.__class__.__name__
        )
        self.logger.info('Criando instancia do Product')


    def parse_solr_xml_status(self, response):
        resultSoup = BeautifulSoup(response)
        status = resultSoup.findAll('int', attrs={'name':'status'})[0].contents[0]
        return status


    def download_product_image(self):
        if not (self.imagem_url is None):
            links = []
            self.logger.info(
                "Product: %s Image URL: %s" % (self.lote,self.imagem_url)
            )
            links.append(self.imagem_url)
            try:
                Download('/home/www/antiquarium_project/static/images/',links)
            except Exception, e:
                self.logger.error("Erro baixando imagem do produto")



    def index_with_solr(self):
        doc_id = self.pId
        doc_lote = self.lote
        doc_desc = self.descricao
        doc_casa = self.casa_leilao
        doc_cat = self.titulo_catalogo
        doc_qual = self.qualificacao

        try:
            doc_desc = unescape(doc_desc)
        except (TypeError, NameError) as e:
            self.logger.error("Erro ao decodificar a DESCRICAO do produto.")
            doc_desc = ""   # TODO: IMPROVE LOOPING XTRACTING CONTENTS UNTIL PURE TEXT.

        # create a connection to a solr server
        # requires: create a shcemaless collecion in solr, named 'jlcs_leiloes':
        ## sudo su - solr -c "/opt/solr/bin/solr create -c jlcs_leiloes -n data_driven_schema_configs"
        try:
            s = Solr('http://antiquarium.com.br:8983/solr/jlcs_leiloes', timeout=10)
            # add a document to the index
            self.logger.info('Adicionando PRODUTO ao índice do Solr.')
            s.add([{
                'pId':doc_id,
                'lote':doc_lote,
                'casa_leilao':doc_casa,
                'descricao':doc_desc,
                'titulo_catalogo':doc_cat,
                'qualificacao':doc_qual
            }])
            self.logger.info('Procedendo ao COMMIT no Solr.')
            s.commit()
            self.logger.info('PRODUTO Indexado no Solr.')
        except socket.error as e:
            if e.errno == errno.ECONNREFUSED:
                self.logger.error('Solr RECUSOU a conexao.')
            else:
                self.logger.error('Solr retornou outro erro de conexao: %s' % e)
                raise
        except SolrError as sE:
            self.logger.error('Solr Error: %s' % str(sE))
            return




    def save_to_redis(self):
        """Saves a product to the local redis database"""
        r = redis.Redis(host="localhost", port=6379, db=0,
            password="c47ee777a92f1a164cc34662aae06fd38e761503f1c2f2c6b7b8d1288a27c1e1", socket_timeout=None, connection_pool=None, charset='utf-8', errors='strict', unix_socket_path=None)

        exists_product = r.sismember("products",self.pId)

        if exists_product:
            #UPDATE
            preco_final_atual = r.get("product:%s:preco_final",self.pId)
            if not (preco_final_atual is None):
                if (len(preco_final_atual) > 2):
                    return True
            self.logger.info("Produto já cadastrado anteriormente. Vou atualizar.")
            return self.upd_product()
        else:
            #CREATE
            self.logger.info("Produto novo. Vou cadastrar.")
            return self.create_product()



    def create_product(self):
        r = redis.Redis(host="localhost", port=6379, db=0,
                password="c47ee777a92f1a164cc34662aae06fd38e761503f1c2f2c6b7b8d1288a27c1e1",
                socket_timeout=None, connection_pool=None, charset='utf-8',
                errors='strict', unix_socket_path=None)
        try:
            #CREATE
            if r.sadd("products", self.pId):
                r.set("product:%s:casa_leilao" % self.pId, self.casa_leilao)
                r.set("product:%s:catalogo_num" % self.pId, self.catalogo_num)
                r.set("product:%s:titulo_catalogo" % self.pId, self.titulo_catalogo)
                r.set("product:%s:qualificacao" % self.pId, self.qualificacao)
                r.set("product:%s:data_hora_coleta" % self.pId, self.data_hora_coleta)
                r.set("product:%s:card_data_hora_leilao" % self.pId, self.card_data_hora_leilao)
                r.set("product:%s:data_hora_leilao" % self.pId, self.data_hora_leilao)
                r.set("product:%s:lote" % self.pId, self.lote)
                try:
                    r.save()
                except ResponseError as r:
                    self.logger.error("Caught ResponseError from redis on save.")
                r.set("product:%s:descricao" % self.pId, self.descricao)
                r.set("product:%s:localizacao" % self.pId, self.localizacao)
                r.set("product:%s:lances" % self.pId, self.lances)
                r.set("product:%s:visitas" % self.pId, self.visitas)
                if not (self.preco_inicial is None):
                    r.set("product:%s:preco_inicial" % self.pId, self.preco_inicial)
                if not (self.preco_final is None):
                    r.set("product:%s:preco_final" % self.pId, self.preco_final)
                r.set("product:%s:url" % self.pId, self.url)
                r.set("product:%s:imagem_url" % self.pId, self.imagem_url)

                # Geração de um hash com base em todas as informações do produto.
                # com o objetivo de acompanhar a evolução do preço do inicio e depois de fechado...
                #hashkey = hashlib.sha1(str(unescape(self)))
                #r.set("product:%s:hashkey" % self.pId, hashkey)
                self.logger.info('Salvando PRODUTO.')
                try:
                    r.save()
                except ResponseError as r:
                    self.logger.error("Caught ResponseError from redis on save.")
                self.logger.info('PRODUTO Salvo no banco de dados.')
                self.download_product_image()
                self.index_with_solr()
                return True
            else:
                self.logger.error('Falhou a inclusão de um novo PRODUTO.')
                return False
        except redis.exceptions.ConnectionError:
            self.logger.error('REDIS Server nao esta disponivel.')
        return False




    def upd_product(self):
        r = redis.Redis(host="localhost", port=6379, db=0,
                password="c47ee777a92f1a164cc34662aae06fd38e761503f1c2f2c6b7b8d1288a27c1e1",
                socket_timeout=None, connection_pool=None, charset='utf-8',
                errors='strict', unix_socket_path=None)
        try:
            if r.sismember("products",self.pId):
                if not (self.preco_final is None):
                    self.logger.info("Salvando novo preco_final: %s " % str(self.preco_final))
                    r.set("product:%s:preco_final" % self.pId, self.preco_final)
                r.set("product:%s:qualificacao" % self.pId, self.qualificacao)
                r.set("product:%s:lances" % self.pId, self.lances)
                r.set("product:%s:visitas" % self.pId, self.visitas)
                self.logger.info('Atualizando PRODUTO.')
                try:
                    r.save()
                except ResponseError as r:
                    self.logger.error("Caught ResponseError from redis on save.")
                self.logger.info('PRODUTO Atualizado no banco de dados.')
                return True
        except redis.exceptions.ConnectionError:
            self.logger.error('REDIS Server nao esta disponivel.')
        return False




#    #Atualizar servidor do SOLR depois de ocnfigurar no DG.
#    @staticmethod
#    def search_for_product_using_solr(needle):
#        # create a connection to a solr server
#        s = Solr('http://antiquarium.com.br:8983/solr/jlcs_leiloes', timeout=10)
#        response = s.query('descricao:%s' % needle)
#        product_list = list()
#        for hit in response.results:
#            # TODO Improve what to be returned here.
#            product_list.append(Product.get_product(hit['id']))
#        return product_list




    @staticmethod
    def get_product(param_id):
        r = redis.Redis(host="localhost", port=6379, db=0,
                password="c47ee777a92f1a164cc34662aae06fd38e761503f1c2f2c6b7b8d1288a27c1e1",
                socket_timeout=None, connection_pool=None, charset='utf-8',
                errors='strict', unix_socket_path=None)
        if r.sismember("products",param_id):
            casa_leilao = r.get("product:%s:casa_leilao" % param_id).decode('utf-8')
            catalogo_num = r.get("product:%s:catalogo_num" % param_id)
            titulo_catalogo = r.get("product:%s:titulo_catalogo" % param_id).decode('utf-8')
            qualificacao = r.get("product:%s:qualificacao" % param_id).decode('utf-8')
            data_hora_coleta = r.get("product:%s:data_hora_coleta" % param_id)
            card_data_hora_leilao = r.get("product:%s:card_data_hora_leilao" % param_id)
            data_hora_leilao = r.get("product:%s:data_hora_leilao" % param_id)
            lote = r.get("product:%s:lote" % param_id)
            descricao = r.get("product:%s:descricao" % param_id).decode('utf-8')
            localizacao = r.get("product:%s:localizacao" % param_id).decode('utf-8')
            lances = r.get("product:%s:lances" % param_id)
            visitas = r.get("product:%s:visitas" % param_id)
            preco_inicial = r.get("product:%s:preco_inicial" % param_id)
            preco_final = r.get("product:%s:preco_final" % param_id)
            url = r.get("product:%s:url" % param_id).decode('utf-8')
            imagem_url = r.get("product:%s:imagem_url" % param_id).decode('utf-8')
            return Product(param_id,
                           casa_leilao,
                           catalogo_num,
                           titulo_catalogo,
                           qualificacao,
                           data_hora_coleta,
                           card_data_hora_leilao,
                           data_hora_leilao,
                           lote,
                           descricao,
                           localizacao,
                           lances,
                           visitas,
                           preco_inicial,
                           preco_final,
                           url,
                           imagem_url)
        else:
            return None


    def __unicode__(self):
        return \
            u'--------------------------------------------------------------------\n' + \
            u'Id: ' + self.pId + u'\n' + \
            u'Casa: ' +  self.casa_leilao + u'\n' + \
            u'Catálogo: ' + self.catalogo_num + u' - ' + unescape(self.titulo_catalogo) + u'\n' + \
            u'Qualificação: ' + (u''if self.qualificacao is None else self.qualificacao) + u'\n' + \
            u'Data Coleta: ' + self.data_hora_coleta + u'\n' + \
            u'Dia #: ' + self.card_data_hora_leilao + u'\n' +  \
            u'Data Leilão: ' + self.data_hora_leilao + u'\n' + \
            u'Lote: ' + self.lote + u'\n' + \
            u'Descricao: ' + unescape(self.descricao) + u'\n' + \
            u'Localizacao: ' + self.localizacao + u'\n' + \
            u'Nro. Lances: ' + self.lances.strip() + u'\n' + \
            u'Nro. Visitas: ' + self.visitas + u'\n' + \
            u'Preco Inicial: ' + self.preco_inicial + u'\n' + \
            u'Preco Final: ' + (u'' if self.preco_final is None else self.preco_final) + u'\n' + \
            u'URL: ' + (u'' if self.url is None else self.url) + u'\n' + \
            u'Imagem: ' + (u'' if self.imagem_url is None else self.imagem_url) + u'\n' + \
            u'--------------------------------------------------------------------\n'



    def __str__( self ):
        return "Not Implemented. Use unicode()."



if __name__ == '__main__':

    """
    module_logger.info( "Procurando um produto usando SOLR...")
    result = Product.search_for_product_using_solr("CASADELEILAO_4000_999899899")
    module_logger.info( str(result))
    """
    module_logger.info( "Criando uma instância de Product...")
    product = Product( \
           "CASADELEILAO_4000_999899899", \
           "Casa de Leilão", \
           "4000", \
           "Obras de Arte", \
           "QUADRO", \
           "10/06/2016 19:00", \
           "1", \
           "16/06/2016 19:00", \
           "375", \
           "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porta nisl a diam aliquet, id porttitor tortor tempor. Nullam non dignissim tortor, eget iaculis justo. Aenean porttitor lectus in pellentesque malesuada. Aliquam consequat eros a sem posuere, eget dignissim leo luctus. Pellentesque maximus interdum facilisis. Pellentesque consequat nisl velit, at venenatis diam ullamcorper vel. Quisque non commodo libero. In quam dui, pulvinar sed dolor eget, fermentum scelerisque mi. Donec consectetur purus et magna facilisis blandit. Nulla id ipsum dignissim, egestas quam ut, auctor elit. Praesent ac ex sit amet erat porta convallis a quis urna. Curabitur vitae fermentum mi.",  \
           "Rio de Janeiro - RJ", \
           "10", \
           "40", \
           "210,00", \
           None, \
           "http://casaDoLeilao/4000/98889988/", \
           "http://casaDeLeilao/4000/98889988/98889988.jpg"
           )



    """
   module_llogger.info( "Indexando... ")
    product.index_with_solr()
    """


    """"
    module_logger.info( "Pesquisando no SOLR...")
    needle = "vaso"
    prod_list = search_for_product_using_solr(needle)
    """




    """
    module_logger.info( "Persistindo Produto em Redis:Product...")
    product.save_to_redis()
    """




    module_logger.info( "Recuperando Produto em Redis:Product...")
    product2 = Product.get_product("galpaodosleiloes_4301_1842317")
    module_logger.info( unicode(product2))


    """
    module_logger.info( "Visualização da instância do Product... ")
    module_logger.info( "========================================")
    module_logger.info( str(product))
    module_logger.info( "========================================")
    """
