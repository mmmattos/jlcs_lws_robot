#!/bin/sh
echo "Monitor de Número de Imagens Baixadas pelo robô."
echo "=============================================== "
find /home/www/antiquarium_project/static/images/ -print | wc -l
echo
echo "Monitor de Número de produtos registrados no banco de dados."
echo "=========================================================== "
echo
redis-cli -a c47ee777a92f1a164cc34662aae06fd38e761503f1c2f2c6b7b8d1288a27c1e1 scard products
echo
echo "Monitor de Escpaço em Disco."
echo "==========================="
echo "Verifique as colunas Avail e Use% para o dispositivo em destaque."
echo
df -h |grep "Avail Use%" && df -h |grep "/dev/vda1"
echo
exit 0
