#!/bin/sh

## Este script limpa (zera) a base:
## - Limpa o Redis
## - Limpa o Solr
## - Remove logs
## - Remove imagens baixadas
##
## Obs.: Deve ser executado por root ou um sudoer.
##
cd /home/antiquarium_robot/scripts
rm /home/logs/*.log 
rm -rf ../www/antiquarium_project/static/images/4*
redis-cli -a c47ee777a92f1a164cc34662aae06fd38e761503f1c2f2c6b7b8d1288a27c1e1 del product
./solr_delete_all.sh
cd .. 
