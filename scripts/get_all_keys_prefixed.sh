#!/bin/bash                                                                                                                 
                                                                                                                            
if [ $# -ne 2 ]                                                                                                             
then                                                                                                                        
  echo "Get keys from Redis matching a pattern using SCAN & GET."                                                           
  ##echo "Usage: $0 <host> <port> <pattern>"                                                                                
  echo "Usage: $0 <pattern> <prefix>"                                                                                                
  exit 1                                                                                                                    
fi                                                                                                                          
                                                                                                                            
cursor=-1                                                                                                                   
keys=""                                                                                                                     
                                                                                                                            
while [ $cursor -ne 0 ]; do                                                                                                 
  if [ $cursor -eq -1 ]                                                                                                     
  then                                                                                                                      
    cursor=0                                                                                                                
  fi                                                                                                                        
                                                                                                                            
  ##reply=`redis-cli -h $1 -p $2 SCAN $cursor MATCH $3`                                                                     
  reply=`redis-cli SCAN $cursor MATCH $1`                                                                                   
  cursor=`expr "$reply" : '\([0-9]*[0-9 ]\)'`                                                                               
  keys=${reply##[0-9]*[0-9 ]}                                                                                               
  ##redis-cli -h $1 -p $2 GET $keys                                                                                         
  if [[ $keys = *[!\ ]* ]]
  then
    echo "$2$keys" 
    redis-cli GET $keys
  fi                                                                                                       

done         
