import logging
import platform


log_filename = "antiquarium_robot.log"
if platform.system() == 'Windows':
    log_filename = '\\Temp\\' + log_filename
elif platform.system() == 'Linux':
    log_filename = 'logs/' + log_filename
lsLogger = logging.getLogger('jlcs_lws_server')
lsHdlr = logging.FileHandler(log_filename)
lsFormatter = logging.Formatter('Leiloes Server %(asctime)s %(levelname)s %(message)s')
lsHdlr.setFormatter(lsFormatter)
lsLogger.addHandler(lsHdlr) 
lsLogger.setLevel(logging.WARNING)
