import os
import logging
import logging.config
import platform
import yaml

logger = logging.getLogger(__name__)

def setup_logging(
    default_path='antiquarium_robot_.log', 
    default_level=logging.INFO,
):
    """
        Setup logging configuration
    """
    path = default_path

    if platform.system() == 'Windows':
        path = '\\Temp\\' + path
    else:
        path = 'logs/' + path

    config_path = './logging.yaml'
    if os.path.exists(config_path):
        print "PRINT: YAML FILE FOUND!"
        with open(config_path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        print "PRINT: NO YAML FILE FOUND!"
        logging.basicConfig(level=default_level)


setup_logging('./logging.yaml', logging.DEBUG)

logger.info("Iniciando o TESTE DE LOG DO LOG do ANTIQUARIUM ROBOT")

logger.info("Encerrando as atividades do TESTE DE LOG DO ANTIQUARIUM ROBOT")
