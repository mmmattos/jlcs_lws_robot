__author__ = 'mmmattos'
#To change this template use Tools | Templates.
#
import os
from time import time
from Queue import Queue
from threading import Thread
from download import setup_download_dir, get_links, download_link
import logging

#logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
#logging.getLogger('requests').setLevel(logging.CRITICAL)
#logger = logging.getLogger(__name__)

module_logger = logging.getLogger('antiquarium_robot.single')

class DownloadWorker(Thread):


    def __init__(self, queue):
        self.logger = logging.getLogger('antiquarium_robot.single.DownloadWorker')
        self.logger.info('Criando instancia do DownloadWorker')
        Thread.__init__(self)
        self.logger.info('Iniciando thread queue')
        self.queue = queue

    def run(self):
        while True:
            #self.logger.info('Got the work from the queue and expand the tuple')
            directory, link = self.queue.get()
            self.logger.info('Got directory: %s' % directory)
            self.logger.info('Got link: %s' % link)
            download_link(directory, link)
            self.queue.task_done()

def Download(dnld_dir,links):
    ts = time()
    ## Create a queue to communicate with the worker threads
    queue = Queue()
    ## Create 8 worker threads
    for x in range(8):
        worker = DownloadWorker(queue)
        ## Setting daemon to True will let the main thread exit even though the workers are blocking
        worker.daemon = True
        worker.start()
    ## Put the tasks into the queue as a tuple
    for link in links:
        catalogo = str(link).split('/')[-2];
        download_dir = setup_download_dir(os.path.join(dnld_dir,catalogo))
        #module_logger.info('Queueing %s' % link)
        queue.put((download_dir, link))
    ## Causes the main thread to wait for the queue to finish processing all the tasks
    queue.join()
    module_logger.info(str('Took {}'.format(time() - ts)))

if __name__ == '__main__':
    dnld_dir = '/home/www/antiquarium_project/static/images/'
    links = get_links()
    Download(dnld_dir, links)
