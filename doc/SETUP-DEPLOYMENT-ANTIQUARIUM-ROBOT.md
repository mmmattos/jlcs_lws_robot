###
### INSTALL GIT
###
sudo apt-get install -y git

## SETUP A BA03RE GIT REPO
$ sudo mkdir /home/git && cd /home/git
$ sudo mkdir antiquarium_robot.git && cd antiquarium_robot.git
$ sudo git init --bare

## EDIT YOUR BASH PROFILE ADDING THIS BRANCH INFO TO THE PROMPT
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\u@\h \W\[\3[32m\]\$(parse_git_branch)\[\033[00m\] $ "

## Configure the Post-Receive Hook
$ sudo cp ../scripts/hooks/post-receive \
            /home/git/antiquarium_robot.git/hooks
## Then make the file executable:
$ sudo chmod +x hooks/post-receive

## BACK TO /home/antiquarium_robot/
git init
sudo git remote add production \
        root@antiquarium.com.br:/home/git/antiquarium_project.git

## MAKE CHANGES ON SOURCE, COMMIT AND PUSH.
## SSH TO PRODUCTION AND FIX PERMISSIONS...
cd /home/antiquarium_robot
sudo chmod +x scripts/*.sh
sudo chown -R antiquarium_robot *
sudo chgrp -Rantiquarium_robot *

## RUN:
$ sudo supervisorctl [start|stop|restart|status] antiquarium_robot
## CLONE THAT REPO ANYWHERE. PUSH COMMITS FROM ANYWHERE. 
## SSH TO RESTART SUPRTVISOR

