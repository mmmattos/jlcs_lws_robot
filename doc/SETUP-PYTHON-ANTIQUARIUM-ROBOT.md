
##
## INSTALL REQUIREMENTS
##
sudo apt-get install -y python python-pip python-virtualenv

##
## SETUP FLASK'S HOME DIRECTORY.
sudo mkdir /home/antiquarium_robot && cd /home/antiquarium_robot
sudo virtualenv env
source robot_env/bin/activate

# INSTALL REQUIREMENTS - GRAB FROM GIT.
sudo env/bin/pip2.7 install -r requirements 

##
## INSTALL PIP MODULES AS SPECIFIED IN THE REQUIREMENTS.TXT FILE.
##
cd src/server && sudo pip install -r requirements.txt  



