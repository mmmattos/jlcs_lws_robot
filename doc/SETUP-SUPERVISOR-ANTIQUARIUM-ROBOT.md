##
## INSTALL
##
sudo apt-get install -y supervisor

##
## Add user for Antiquarium operation.
##
sudo adduser antiquarium_robot
sudo adduser antiquarium_robot sudo

## Create the app logs directory.
sudo mkdir /home/logs
sudo chmod 777 /home/logs

# Add web interface to /etc/supervisor/supervisord.conf
[inet_http_server]
port = 9001
username = admin
password = tranhsbc1108

#then do: $ sudo service supervisor restart

## MANAGE SUPERVISOR PROCESS
## =========================
sudo service supervisor [ start | restart | stop | status ]


## CONFIGURE SUPERVISOR
## ==================== 
sudo vim /etc/supervisor/conf.d/antiquarium_robot.conf
## Add contents from  src/robot/conf/antiquarium_robot.conf


## Start the robot with supervisor:
## ================================
$ sudo supervisorctl reread
$ sudo supervisorctl update
$ sudo supervisorctl start antiquarium_robot

