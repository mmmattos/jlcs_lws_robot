# -*- encoding: utf-8 -*-
import re
import htmlentitydefs
import mechanize
import urllib2
import cookielib
from BeautifulSoup import BeautifulSoup
import html2text
import htmllib
import redis
import datetime
import time
import logging
from product import Product
from tools import unescape

class leiloes_crawler:
    """
    A Class to crawl based initially in the Leiloes-BR standard site.
    :copyright: (c) 2016 by Miguel Miranda de Mattos.
    :license: 
    """

    
    def get_soup_for_me(self, browser, url):
        try:
            browser.open(url)
            assert browser.viewing_html()
            html = browser.response().read()
            result_soup = BeautifulSoup(html)
        except urllib2.URLError as e:
            self.logger.error("ERROR - Open URL %s " % url)
        #result_soup = BeautifulSoup(html.decode('utf-8','ignore'))
        #result_soup = BeautifulSoup(html, fromEncoding='latin-1')
        #print result_soup.prettify('windows-1252')
        #quit()
        return result_soup



    
    def get_product_description_from_soup(self,description_soup):
        for a in description_soup:
            try:
                product_desc = a.findAll('p')[0].contents[0]
            except Exception, e:
                product_desc = ''
                self.logger.warning( "Could not get description from: %s", description_soup)

        #TODO Recursively exhaust getting soups until just text is left here. NO TAGS.
        return product_desc




    def product_filter(self, tag):
        return (tag.name == 'li' and
            tag.parent.name == 'ul' and
            'is-List' in tag.parent['class'] and
            'lista' in tag.parent['id'])




    def crawl(self):
        # Register regexps to be used globally
        patFinderTitle = re.compile(r'<a[^>]*\stitle="(.*?)"')
        patFinderHref = re.compile(r'<a[^>]*\shref="(.*?)"')
        
        # Fire-up a new  browser session anf get tag soup for the page.
        br = mechanize.Browser()
        for llao_url in self.base_urls:
            mainsoup = self.get_soup_for_me(br,llao_url)

            # Casa do Leilão.
            casa_leilao = mainsoup.findAll('title'
                )[0].contents[0]
            print str(casa_leilao)

            root_url = llao_url.split('/')
            main_url = root_url[0]+root_url[1]+root_url[2]
            main_url = main_url.split(':')[0]+'://'+main_url.split(':')[1]
            self.logger.info("URL base: %s", str(main_url))
            
            casa_leilao_id = main_url.split('.')[1]

            catalogo_num = root_url[-1].split('=')[1]
            print 'Catálogo Num.:' + str(catalogo_num)
            #self.logger.info("Achou Casa de Leilão: %s", casa_leilao)
            #self.logger.info("Catálogo Num.: %s", catalogo_num)


            # Títulos dos Catálogos        
            titulo_catalogo = ""
            titulo_catalogo_soup = mainsoup.findAll('div',attrs  = {'class': 'cat-title'})
            if len(titulo_catalogo_soup) > 0:
                titulo_catalogo = titulo_catalogo_soup[0].findAll('span',attrs = {
                    'class': 'realtitle tipo-75 float-left'
                })[0].contents[0]


            # Meta-dados        
            '''
            titulo_catalogo_soup = mainsoup.findAll('meta', attrs={'property': 'og:title'})[0]
            titulo_catalogo = repr(titulo_catalogo_soup['content'])
            '''
            a = unescape(titulo_catalogo)
            print a
            #self.logger.info("Achou Catálogo: %s", titulo_catalogo)

            '''
            meta_soup = mainsoup.findAll('meta', attrs={'property': 'og:description'})[0]
            data_leilao = meta_soup['content'].split(':')[1]
            print str(data_leilao)
            '''

            d = datetime.datetime.strptime( "30/05/2016", "%d/%m/%Y" )                                                                    
            delta = datetime.timedelta(days=1)                                                                                            
            t = datetime.datetime.strftime(datetime.datetime.today(),"%d/%m/%Y")                                                          
            t = datetime.datetime.strptime(str(t),"%d/%m/%Y")                                                                             
            #if (d + delta == t)

            data_hora_coleta = t

            try:
               # Dia(s) do Leilão
               dias_soup = mainsoup.findAll('input', attrs={'type': 'radio','name': 'dia'}) 
               for d in dias_soup:
                   card_dia = str(d['value'])
                   #sDia = "Dia " + card_dia
                   label_id = d['id']
                   label_soup = mainsoup.find('label',attrs = {'for': label_id})
                   data_leilao = label_soup.find('a').contents[0]
                   #sDia = sDia + str(str(data_leilao).split('-')[1]) + str(str(data_leilao).split('-')[2])
                   #print 'Data Leilao: ' + str((data_leilao.split('-')[1]).strip())
                   data_leilao_parts = data_leilao.split('-')
                   data_hora_leilao = str(data_leilao_parts[1] + ' - ' + data_leilao_parts[2])
                   print str(casa_leilao) + ":" + str(titulo_catalogo) + ":INICIANDO CAPTURA DO DIA #" + str(card_dia) 
                   print 'Data Hora Leilao: ' + str(data_hora_leilao)
                   new_url = llao_url + "&Dia=" + card_dia 
                   self.logger.info('Url com Dia: %s',  str(new_url))
                   keep_reading = 1
                   mainsoup = self.get_soup_for_me(br,new_url)
   
                   while not (mainsoup is None):
                       self.logger.info('GOT MAINSOUP!')
   
                       # Lista de Produtos Leiloados.
                       lista_soup = mainsoup.findAll('ul', attrs={'class': 'is-List', 'id': 'lista'})
                       #self.logger.info('Lista Soup: %s ' % lista_soup)
                       if len(lista_soup) == 0:
                           raise ValueError('Lista de produtos fora do padrão.Criar novo padrão para: {} '.format(llao_url))
                       product_soup = lista_soup[0].findAll(self.product_filter)
                       if len(product_soup) == 0:
                           self.logger.info(str( "A lista de produtos esta vazia!"))
                           break
                       else:
                           contadores_soup = mainsoup.findAll('div', attrs={'class': 'results'})
                           contadores = str(contadores_soup[0].find('p').find('strong').contents[0]).strip().split('-')
                           #print contadores
                           cont_ppg = contadores[0].strip()
                           cont_tot = contadores[1].strip()
   
                           #print "Por Pagina : " + str(cont_ppg)
                           #print("Total produtos : %s" % str(cont_tot))
                           self.logger.info("Na página: %s",str(min(cont_ppg,len(product_soup))))
                           print unicode("Total de produtos na pag.: {}".format(len(product_soup)))
   
                           for pr in product_soup:
                               preco_inicial = None
                               preco_final = None
   
                               # ContentProd (Inclui todos os elementos menos a URL da Imagem)
                               cp_soup = pr.findAll('div', attrs={'class': 'ContentProd is-Right'})
                               for cp in cp_soup:
   
                                   # LOTE
                                   lote_soup = cp.findAll('div', attrs={'class': 'LoteProd'})
                                   for lp in lote_soup:
                                       lote = unicode((lp.findAll('a')[0].contents[0]).contents[0])
                                       self.logger.info('Num.Lote: %s', lote)
   
   
                                   #
                                   # URL da Peça
                                   # 
                                   try:
                                       peca_soup = cp.find('h3', attrs={'class': 'TitleProd'})
                                       peca_url = str(peca_soup.find('a')['href'])
                                       peca_id = ((peca_url.split('?')[1]).split('&')[0]).split('=')[1]
                                       self.logger.info("Peça: %s",str(peca_id))
                                   except Exception, e:
                                       self.logger.info("Erro capturando a URL da Peça leiloada!")
   
   
                                   #
                                   # LANCES
                                   # 
                                   try:
                                       lances_soup = cp.find('span', attrs = {'id': 'Q*00*'})
                                       lances = lances_soup.find('strong').contents[0]
                                       lances = str(lances).strip()
                                       self.logger.info("Lances: %s", lances)
                                   except Exception, e:
                                       self.logger.info("Erro capturando Número de Lances!")
   
                                   # 
                                   # VISITAS
                                   # 
                                   try:
                                       visitas_soup = cp.find('div', attrs = {'class': 'PrecoProd'})
                                       visitas = visitas_soup.findAll('span')[1].contents[0]
                                       visitas = str(visitas).strip()
                                       self.logger.info("Visitas: %s" % visitas)
                                   except Exception, e:
                                       self.logger.info("Erro capturando Número de Visitas!")
   
   
                                   #
                                   #  LOTE VENDIDO?
                                   #  
                                   lote_vendido = None
                                   try:
                                       lote_vendido_soup = None
                                       lote_vendido_soup = cp.find('div', attrs = {'class': 'lotevendido'})
                                       if not lote_vendido_soup is None:
                                           lote_vendido = lote_vendido_soup.find('span').contents[0]
                                           if lote_vendido == "Lote Vendido":
                                               self.logger.info(lote_vendido)
                                   except Exception, e:
                                       self.logger.info("Erro capturando se Lote foi Vendido! Status indefinido!")
                                       lote_vendido = "Indefinido"
   
   
   
                                   # 
                                   # DETALHES DA PEÇA
                                   # 
                                   try:
                                       detalhe_url = main_url + "/" + peca_url 
                                       self.logger.info("URL do Lote: %s",detalhe_url)
                                       detalhe_soup = self.get_soup_for_me(br, detalhe_url)
   
   
   
                                       #
                                       #  QUALIFICAÇÃO
                                       #  
                                       try:
                                           qualif_soup = detalhe_soup.find('p', attrs = {'class': 'tipo'})
                                           qualificacao = None
                                           qualificacao = (qualif_soup.find('span')).find('a').contents[0]
                                           if not qualificacao is None:
                                               self.logger.info("Qualificação: %s",str(qualificacao).strip())
                                       except Exception, e:
                                           self.logger.info("Erro capturando Qualificação do lote " + lote)    
   
                                       # 
                                       # DESCRIÇÃO
                                       # 
                                       try:
                                           descr_soup = detalhe_soup.find('div', attrs = {'id': 'produto'})
                                           descricao = None
                                           descricao = descr_soup.find('p').contents[0]
                                           if not descricao is None: 
                                               self.logger.info("Descrição: %s", str(descricao))
                                       except Exception, e:
                                           self.logger.info("Erro capturando Descrição do lote %s",lote)
   
                                       # 
                                       # LOCAL
                                       # 
                                       try: 
                                           local_soup = detalhe_soup.find('li', attrs = {'class': 'campolocal'})
                                           localizacao = None
                                           localizacao = (local_soup.find('p')).find('a').contents[0]
                                           if not localizacao is None:
                                               self.logger.info("Localização: %s",str(localizacao))
                                       except Exception, e:
                                           self.logger.info("Erro capturando Localização do lote %s ", lote)
   
   
                                       # 
                                       # VALOR INICIAL OU ATUAL (FINAL)
                                       # 
                                       try:
                                           valor_soup = detalhe_soup.find('li', attrs = {'class': 'small-info'})
                                           valor = None
                                           #print str(valor_soup)
                                           if len((valor_soup.find('ul')).findAll('li')) >= 4:
                                               valor = ((valor_soup.find('ul')).findAll('li')[3]).find('p').contents[0]
                                               if not valor is None: 
                                                   if lote_vendido is None:
                                                       lblValor = u'Valor Inicial: '
                                                       preco_inicial = str(valor)
                                                   elif lote_vendido == u'Lote Vendido':
                                                       lblValor = u'Valor Atual: '
                                                       preco_final = str(valor)
                                                   else:
                                                       lblValor = u'Valor: '
                                                   valor = unicode(valor).strip()
                                                   self.logger.info("%s %s",lblValor,valor)
                                       except Exception, e:
                                           print u'Caught EXCEPTION in line 295.'
                                           self.logger.info("Erro capturando Valor Inicial do lote %s",lote)
   
                                   except Exception, e:
                                       print u'Caught EXCEPTION in line 298'
                                       self.logger.info("Erro recuperando página da peça do lote %s",lote)
       
                                   self.logger.info('----------------------------------------------------------')
                                   #self.logger.info('Lote: {0} - {1} - {2}: {3} '.format(lote, data_hora_leilao, lblValor, valor))
                                   self.logger.info("Descrição: %s" % str(descricao))          
                                             
                               # URL da IMAGEM
                               img_url = None
                               try:
                                   imgp_soup = pr.findAll('div', attrs={'class': 'ImagemProd is-Left'})[0]
                                   img_url = imgp_soup.findAll('img')[0]['src']
                                   self.logger.info(str('URL da Imagem: %s' % img_url))
                               except Exception, e:
                                   self.logger.error(str("Não consegui pegar URL Imagem: %s" % repr(imgp_soup.contents[0])))
   
                               #
                               # Persiste Produto
                               #
                                
                               product_id = casa_leilao_id + '_' + catalogo_num + '_' + peca_id
                               
                               product = Product( \
                                                 product_id, \
                                                 casa_leilao, \
                                                 catalogo_num, \
                                                 titulo_catalogo, \
                                                 str(qualificacao).strip(), \
                                                 data_hora_coleta, \
                                                 card_dia, \
                                                 data_hora_leilao,  \
                                                 lote, \
                                                 descricao, \
                                                 localizacao, \
                                                 lances, \
                                                 visitas,
                                                 preco_inicial, 
                                                 preco_final,\
                                                 detalhe_url, \
                                                 img_url)
                                                
                                                 
   
                               #self.logger.info(unescape(product))
   
                               product.save_to_redis()
   
                       #print str(cont_ppg)
                       #print str(len(product_soup))
                       #print str(cont_tot)
                       if min(cont_ppg,len(product_soup)) >= cont_tot:
                           keep_reading = 0
                           mainsoup = None
                       else:
                           new_url = llao_url + "&Dia=" + str(card_dia) + "&PgI=" + str(cont_ppg)
                           mainsoup = self.get_soup_for_me(br,new_url)

               print str(casa_leilao) + ":" + str(titulo_catalogo) + ":CONCLUINDO CAPTURA DO DIA #" + str(card_dia) 

            except ValueError as error:
               self.logger.error('Error: {0}'.format(error))





    def __init__(self):
        
        self.logger = logging.getLogger(
          self.__module__ + '.' + 
          self.__class__.__name__
        )
        self.logger.info('Criando instancia do Crawler')

        #
        # Grab base urls to explore.
        #
        
        self.base_urls = []
        self.leiloes_url = "http://www.leiloesbr.com.br"
        br = mechanize.Browser()
        mainsoup = self.get_soup_for_me(br,self.leiloes_url)
        
        leiloes_soup = mainsoup.findAll('h3')

        self.logger.info(
          "Encontrei estes %d Catálogos em LeiloesBR.", 
          len(leiloes_soup)
        )

        for llao in leiloes_soup:
            llao_parts = (llao.find('a')['href']).split('|')
            llao_url = llao_parts[1]+'/catalogo.asp?Num='+llao_parts[2]
            self.logger.info("URL: %s", str(llao_url) )
            if self.base_urls.__contains__(llao_url):
                continue
            self.base_urls.append(llao_url)
            print str(llao_url)
        

        MAX_CONSECUTIVE_ATTEMPTS = 500
        self.logger.info("Base URLs set: %s", str(self.base_urls))

