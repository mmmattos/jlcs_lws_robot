# -*- encoding: utf-8 -*-
__author__ = 'mmmattos'
#To change this template use Tools | Templates.
#
## image checker.
import os
import redis
from download import setup_download_dir, Download 


"""
def check_n_dnld_missing():
    products = r.smembers("products")
    how_many = products.__len__()
    print "Checking images..."
    print "Found " + str(how_many) + " products."
    i = 0 
    d = 0
    for key in r.keys('product:*:imagem_url'):
        i = i + 1
        if d > 5000:
            print "Reached limit... Jumping out..."
            break
        catalogo = str(r.get(key)).split('/')[-2];
        filename = str(r.get(key)).split('/')[-1];
        download_file = os.path.join(dnld_dir,catalogo,filename)
        if not os.path.isfile(download_file):
            links = []
    	    links.append( str(r.get(key)) )
            Download(dnld_dir, links) 
            d = d + 1 
            print "Ate agora " + str(d) + " downloads de " + str(i) + " arquivos. "
            print " --> " + key + " --> " + r.get(key)
            print "file was missing: Downloaded now...!"
"""

def download_missing(links):
    for link in links:
        links2download = []
        links2download.append(link)
        Download(dnld_dir, links2download)


def check_missing():
    products = r.smembers("products")
    how_many = products.__len__()
    print "Checking images..."
    print "Found " + str(how_many) + " products."
    i = 0
    links = []
    for key in r.keys('product:*:imagem_url'):
        catalogo = str(r.get(key)).split('/')[-2];
        filename = str(r.get(key)).split('/')[-1];
        download_file = os.path.join(dnld_dir,catalogo,filename)
        if not os.path.isfile(download_file):
            links.append(str(r.get(key)))
            i = i + 1
    print "Found %s missing files..." % str(i)
    #print "Links: "+str(links)
    if i > 0:
        download_missing(links)


if __name__ == '__main__':
    dnld_dir = '/home/www/antiquarium_project/static/images/'
    r = redis.Redis(host="localhost", port=6379, db=0,
        password="c47ee777a92f1a164cc34662aae06fd38e761503f1c2f2c6b7b8d1288a27c1e1",
        socket_timeout=None, connection_pool=None, charset='utf-8',
        errors='strict', unix_socket_path=None)
    check_missing()
