__author__ = 'mmmattos'
#To change this template use Tools | Templates.
#
import logging
import re

"""
@staticmethod
def unescape(s):
    p = htmllib.HTMLParser(None)
    p.save_bgn()
    p.feed(s)
    return p.save_end
"""


logger = logging.getLogger(__name__)

##
# Removes HTML or XML character references and entities from a text string.
#
# @param text The HTML (or XML) source text.
# @return The plain text, as a Unicode string, if necessary.
def unescape(text):
    def fixup(m):
        text = m.group(0)
        if text[:2] == "&#":
            # character reference
            try:
                if text[:3] == "&#x":
                    return unichr(int(text[3:-1], 16))
                else:
                    return unichr(int(text[2:-1]))
            except ValueError:
                logger.warning("WARNING - UNESCAPE Failed!")
                return "******************************"
        else:
            # named entity
            try:
                text = unichr(htmlentitydefs.name2codepoint[text[1:-1]])
            except KeyError, NameError:
                logger.warning("WARNING - UNESCAPE Failed!")
                return "******************************"
        return text # leave as is
    return re.sub("&#?\w+;", fixup, text)
